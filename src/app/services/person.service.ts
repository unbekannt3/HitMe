import { Injectable } from '@angular/core';

@Injectable()
export class PersonService {

  name = '';
  size = 300;
  url = 'https://api.adorable.io/avatars/' + this.size + '/';


  constructor() { }

  saveName(_name: string) {
    this.name = _name;
  }

  setSize(_size: number) {
    this.size = _size;
  }

  get getImageURL() {
    const nameURL = this.name.split(' ').join('+');
    let url = 'https://api.adorable.io/avatars/' + this.size + '/' + nameURL + '.png';
    if (this.name.toUpperCase() === 'LUCAS') {
      url = 'assets/lucas.png';
    } else if (this.name.toUpperCase() === 'LUCASS' || 'HURENSOHN' || 'KEVIN' || 'KNOPPERS') {
      url = 'assets/kevin.png';
    }
    return url;
  }

  get getName() {
    return this.name;
  }

  get getSize() {
    return this.size;
  }

}
