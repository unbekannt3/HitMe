import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertModule } from 'ngx-alerts';

import { AppComponent } from './app.component';
import { NameInputComponent } from './components/name-input/name-input.component';
import { HitPersonComponent } from './components/hit-person/hit-person.component';
import { PersonService } from './services/person.service';
import { FooterComponent } from './components/footer/footer.component';

const appRoutes: Routes = [
  {
    path: 'enterName',
    component: NameInputComponent,
    data: { title: 'Namen eingeben - Schlag den _____' }
  },
  {
    path: 'hitMe',
    component: HitPersonComponent,
    data: { title: 'Neues Todo  - Schlag den _____' }
  },
  {
    path: '',
    redirectTo: '/enterName',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NameInputComponent,
    HitPersonComponent,
    FooterComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    AlertModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [PersonService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
