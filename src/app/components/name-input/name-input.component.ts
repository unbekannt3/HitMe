import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../services/person.service';
import { Router } from '@angular/router';
import { AlertService } from 'ngx-alerts';

@Component({
  selector: 'app-name-input',
  templateUrl: './name-input.component.html',
  styleUrls: ['./name-input.component.scss']
})
export class NameInputComponent implements OnInit {

  nameFieldValue = '';

  constructor(
    private personService: PersonService,
    private alertService: AlertService,
    private router: Router
  ) { }

  setName() {
    this.personService.saveName(this.nameFieldValue);
    this.routeToHitPage();
  }

  isNullOrWhiteSpace(str) {
    return (!str || str.length === 0 || /^\s*$/.test(str));
  }

  routeToHitPage() {
    if (this.isNullOrWhiteSpace(this.nameFieldValue)) {
      this.alertService.danger('You must enter a name here first!');
    } else {
      this.router.navigate(['/hitMe']);
    }
  }

  ngOnInit() {
  }

}
