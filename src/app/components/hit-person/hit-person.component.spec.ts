import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HitPersonComponent } from './hit-person.component';

describe('HitPersonComponent', () => {
  let component: HitPersonComponent;
  let fixture: ComponentFixture<HitPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HitPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HitPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
