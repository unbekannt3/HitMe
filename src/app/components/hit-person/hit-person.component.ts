import { Component, OnInit, HostListener } from '@angular/core';
import { PersonService } from '../../services/person.service';
import { AlertService } from 'ngx-alerts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hit-person',
  templateUrl: './hit-person.component.html',
  styleUrls: ['./hit-person.component.scss'],
})
export class HitPersonComponent implements OnInit {

  hits = 0;
  name = 'asd';
  image = 'asd';

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    this.showHitAlert();
  }

  constructor(
    private personService: PersonService,
    private alertService: AlertService,
    private router: Router
  ) { }

  showHitAlert() {
    this.alertService.success('Yay! You\'ve hit ' + this.name + ' :)');
    this.hits += 1;
  }

  get inputName(): string {
    return this.name;
  }

  get imageURL(): string {
    return this.image;
  }

  get currentHits(): number {
    return this.hits;
  }

  ngOnInit() {
    this.name = this.personService.getName;
    this.image = this.personService.getImageURL;

    if (this.name === '') {
      this.router.navigate(['/enterName']);
    }
  }

}
